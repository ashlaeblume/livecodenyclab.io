---
name: Live Code Lab (Workshop)
venue: NYU Polytech
date: 2019-02-02 11:30 AM
link: https://www.facebook.com/events/271161637076761/
image: /image/livecodelab.jpg
---

Magnet at NYU Polytechnic School of Engineering
2 Metrotech, 8th Floor, Brooklyn, NY, 11201, Brooklyn, New York

LiveCode.NYC and [Music Community Lab](https://www.facebook.com/MusicCommunityLab/) ([Monthly Music Hackathon NYC](https://www.facebook.com/musichackathon/)) present a day exploring the intersection of live code, music/sonic art, visuals, and beyond.

Featuring talks and workshops from artists and creators of live code performance tools, as well as open time for discussion, hacking, collaboration, serendipity, and performance.
