---
name: The New Wave of Live Code
venue: Babycastles
date: 2019-08-02 7:00 PM
link: https://t.co/T3ZUe6yNXv
image: https://pbs.twimg.com/media/D92w3SSXYAMQzSI.jpg:large
---

Livecode.nyc New Artist Showcase
Looking for artists for a new artist showcase on August 2nd at Babycastles. We're looking for both mentors and artists to play this show or helping new artists prep for this show.
