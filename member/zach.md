---
name: Zach Krall
image: https://github.com/zachkrall.png
links:
    website: https://zachkrall.com/
    twitter: https://twitter.com/zachkralli
    mastodon: https://merveilles.town/@zachkrall
---

Brooklyn based artist, waiting to be uploaded to the cloud.
