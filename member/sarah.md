---
name: Sarah Groff Hennigh-Palermo
image: http://sarahghp.com/img/about/headshot.png
links:
    website: http://sarahghp.com/
---

Brooklyn-based artist and programmer. Looking for the answer to _What if Malevich, Lichtenstein, and Martin had a baby and this baby learned to code?_ Graphix with [Codie](https://twitter.com/hi_codie).
