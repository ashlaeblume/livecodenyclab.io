---
name: Ashlae Blume
image: http://github.com/ashlaeblume.png
links:
  soundcloud: https://soundcloud.com/ukiyobang
  bandcamp: https://bandcamp.com/ashlaeblume
  instagram: https://instagram.com/laelume
  twitter: https://twitter.com/ashlaeblume
  website: about.me/ashlaeblume
---

mad scientist with a penchant for the unknown.
