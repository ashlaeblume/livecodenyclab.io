---
name: Melody Loveless
image: https://avatars0.githubusercontent.com/u/5409179?s=460&v=4
links:
    instagram: https://www.instagram.com/melodycodes/
---

Musician, multimedia artist, educator, and performer. Sometimes sings and codes at the same time. Uses Sonic Pi and Max/MSP/Jitter often but is always trying something new.
