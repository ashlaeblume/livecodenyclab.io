﻿---
name: Maxwell Neely-Cohen
image: https://github.com/mncmncmnc.png
links:
    website: https://www.maxwellneelycohen.com/
    twitter: https://twitter.com/nhyphenc
---

Writer who procrastinates by experimenting with technology
